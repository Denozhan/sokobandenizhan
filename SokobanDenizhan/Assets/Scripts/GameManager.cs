using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public AudioSource levelCompletedSFX;
    public Canvas levelCompletedCanvas;
    public Canvas pauseMenuCanvas;
    public int levelNum;

    private GameObject[] _redCubePoints;
    private GameObject[] _greenCubePoints;
    private bool _redPointCompleted;
    private bool _greenPointCompleted;
    private bool _levelCompleted;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        PlayerController.playerController.dragSFX.Stop();
        levelCompletedCanvas.gameObject.SetActive(false);
        pauseMenuCanvas.gameObject.SetActive(false);
        _levelCompleted = false;
        findAllCubePoints();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Time.timeScale = 0f;
            pauseMenuCanvas.gameObject.SetActive(true);
        }
        checkForLevelCompletion();
    }

    void checkForLevelCompletion()
    {
        if (!_levelCompleted)
        {
            _redPointCompleted = true;
            _greenPointCompleted = true;

            foreach (GameObject go in _redCubePoints)
            {
                if (!go.GetComponent<CubePoint>()._completed)
                {
                    _redPointCompleted = false;
                }
            }

            foreach (GameObject go in _greenCubePoints)
            {
                if (!go.GetComponent<CubePoint>()._completed)
                {
                    _greenPointCompleted = false;
                }
            }

            if (_redPointCompleted && _greenPointCompleted)
            {
                _levelCompleted = true;
                PlayerController.playerController.dragSFX.Stop();
                levelCompletedSFX.Play();
                Invoke("levelCompleted", 0.25f);
                levelCompletedCanvas.gameObject.SetActive(true);
            }
        }
    }

    void levelCompleted()
    {
        Time.timeScale = 0f;
        PlayerPrefs.SetInt("level", levelNum+1);
        Debug.Log("Level Completed.");
    }

    void findAllCubePoints()
    {
        _redCubePoints = GameObject.FindGameObjectsWithTag("RedCubePoint");
        _greenCubePoints = GameObject.FindGameObjectsWithTag("GreenCubePoint");
    }
}
