using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    public Canvas pauseGameCanvas;

    public void ClickRetry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ClickNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }

    public void ClickContinue()
    {
        Time.timeScale = 1f;
        pauseGameCanvas.gameObject.SetActive(false);
    }

    public void ClickExit()
    {
        Application.Quit();
    }
}
