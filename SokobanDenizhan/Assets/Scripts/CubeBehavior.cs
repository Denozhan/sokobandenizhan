using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeBehavior : MonoBehaviour
{
    public float playerForce = 0.01f;
    public float gravityValue = -9.81f;
    public float sphereRadius;
    [Header("Counted or Not")]
    public bool isCountedBlock = false;
    [Tooltip("Necessary if is counted block.")]
    public int countNumber = 0;

    public LayerMask whatIsGround;
    public LayerMask whatAreCube;

    public Canvas countCanvas;
    public Text counterText;

    public Transform groundCheckPosition;

    private Rigidbody _rigidbody;
    private bool _isGrounded;

    private bool _canMove;
    private Vector3 _directionToMove;

    // Start is called before the first frame update
    void Start()
    {
        _isGrounded = true;
        _canMove = false;
        if (isCountedBlock)
        {
            counterText.text = countNumber.ToString();
            countCanvas.gameObject.SetActive(true);
        }
        else
        {
            countCanvas.gameObject.SetActive(false);
        }
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Physics.CheckSphere(groundCheckPosition.position, sphereRadius, whatIsGround))
        {
            _isGrounded = false;
        }
        else
        {
            _isGrounded = true;
        }

        if (!_isGrounded)
        {
            _rigidbody.AddForce(new Vector3(0, gravityValue, 0), ForceMode.Acceleration);
        }
    }

    private void FixedUpdate()
    {
        if (_canMove && checkObstacles())
        {
            _rigidbody.MovePosition(transform.position + _directionToMove);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "GreenCube" || collision.gameObject.tag == "RedCube")
        {
            _canMove = false;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
        }
        else if (collision.gameObject.tag == "Player")
        {
            _canMove = true;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            if (isCountedBlock)
            {
                if (countNumber > 0)
                {
                    countNumber -= 1;
                    Debug.Log(countNumber);
                    counterText.text = countNumber.ToString();
                    Vector3 dir = collision.contacts[0].point;
                    _directionToMove = dir.normalized * Time.deltaTime * playerForce;
                } 
                else
                {
                    _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
                }
            }
            else
            {
                Vector3 dir = collision.contacts[0].point;
                _directionToMove = dir.normalized * Time.deltaTime * playerForce;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (!PlayerController.playerController.isColliding & (collision.gameObject.tag == "GreenCube" || collision.gameObject.tag == "RedCube"))
        {
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(groundCheckPosition.position, sphereRadius);
        Gizmos.DrawCube(transform.position, transform.localScale * 1.25f);
    }

    bool checkObstacles()
    {
        Collider[] colliders = Physics.OverlapBox(transform.position, transform.localScale*1.25f, Quaternion.identity, whatAreCube);
        foreach (Collider cold in colliders)
        {
            if (!GameObject.ReferenceEquals(cold.gameObject, gameObject))
            {
                return false;
            }
        }
        return true;
    }
}
