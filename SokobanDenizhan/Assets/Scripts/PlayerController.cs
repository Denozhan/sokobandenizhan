using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController playerController;

    public float playerSpeed = 2.0f;
    public float gravityValue = -9.81f;
    public AudioSource dragSFX;

    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;

    [HideInInspector]
    public bool isColliding;
    [HideInInspector]
    public GameObject collidingObject;

    private void Start()
    {
        if (playerController == null)
        {
            playerController = GetComponent<PlayerController>();
        }

        isColliding = false;
        controller = gameObject.GetComponent<CharacterController>();
    }

    void Update()
    {
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        dragSFX.Play();
    }

    private void OnCollisionStay(Collision collision)
    {
        collidingObject = collision.gameObject;
        isColliding = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        isColliding = false;
        collidingObject = null;
        dragSFX.Stop();
    }

}