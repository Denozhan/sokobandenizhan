using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePoint : MonoBehaviour
{
    [HideInInspector]
    public bool _completed;

    public string cubeType;

    // Start is called before the first frame update
    void Start()
    {
        _completed = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(cubeType == "Red")
        {
            if (other.gameObject.tag == "RedCube")
            {
                _completed = true;
            }
        } 
        else if (cubeType == "Green")
        {
            if (other.gameObject.tag == "GreenCube")
            {
                _completed = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _completed = false;
    }
}
