﻿using UnityEngine;

public class MusicController : MonoBehaviour
{
    private AudioSource _audioSource;
    public static MusicController musicController;

    private void Awake()
    {
        if (musicController == null)
            musicController = this.GetComponent<MusicController>();

        _audioSource = this.GetComponent<AudioSource>();
        DontDestroyOnLoad(transform.gameObject);
    }

    public void PlayMusic()
    {
        if (_audioSource.isPlaying) { return; }
        _audioSource.Play();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }
}