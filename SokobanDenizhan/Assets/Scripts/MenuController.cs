using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Button continueButton;        

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetString("unity.player_session_count") == "1")
        {
            PlayerPrefs.SetInt("started", 0);
            PlayerPrefs.SetInt("level", 1);
            continueButton.GetComponent<Button>().interactable = false;
        } 
        else
        {
            if (PlayerPrefs.GetInt("level") > 1 && PlayerPrefs.GetInt("started") == 1)
            {
                continueButton.GetComponent<Button>().interactable = true;
            } else
            {
                continueButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    public void ClickNewGame()
    {
        PlayerPrefs.SetInt("level", 1);
        PlayerPrefs.SetInt("started", 1);
        SceneManager.LoadScene(PlayerPrefs.GetInt("level"));
    }

    public void ClickContinue()
    {
        SceneManager.LoadScene(PlayerPrefs.GetInt("level"));
    }

    public void ClickExit()
    {
        Application.Quit();
    }

}
