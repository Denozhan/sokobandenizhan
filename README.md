# Sokoban - Game Case
---
#### .: Gameplay Video :.
[![Sokoban - Gameplay](https://i.ibb.co/9gtkGBm/Ads-z.png)](https://youtu.be/JAd7CD386mk)

- Use WASD keys to move.
- You can push red or green cubes.
- You need to push cubes to the releated color points to complete the level.
- Game has auto-save feature after every level. If you pass a level, it will automatically save.
- If you go through holes (grey colored tiles), game will be load same level again.

## Features

- Auto-save after every level.
- Unity Physics not used in the game. (gravity, pushing etc.)
- ESC key will display pause menu.
- Cubes can not move if there is a cube in the move direction.

## Credits

- Music and sound effects are royalty free.
- Coding & Game Design by Denizhan Aras
- This case is given by Proud Dinosaurs Game Studio.

## License

This is a game case, so you can not use anything releated to this project.
